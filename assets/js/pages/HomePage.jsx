import React from 'react';
import { Link } from 'react-router-dom';

const HomePage = (props) => {
    return ( 
        <>
            <title>PucesNautiques - Spécialiste depuis plus de 20 ans dans la vente de canoë kayak</title>
            <div className="container pt-5">
                <div className="jumbotron">
                    <h1 className="display-4">Les Puces Nautiques</h1>
                    <p className="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                    <hr className="my-4"/>
                    <p>It uses utility classNamees for typography and spacing to space content out within the larger container.</p>
                    <p className="lead">
                        <Link to='/annonce/formulaire/new' className="btn btn-info">Créer une annonce</Link>
                    </p>
                </div>
            </div>
        </>
  );
}
 
export default HomePage;