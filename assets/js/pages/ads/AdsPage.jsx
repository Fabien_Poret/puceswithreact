import React, { useEffect, useState } from 'react';
import adsAPI from '../../services/adsAPI';
import Pagination from '../../components/Pagination';
import moment from "moment";
import {Link} from "react-router-dom";
import AdsLoader from '../../components/loaders/AdsLoader';
import InfiniteScroll from "react-infinite-scroll-component";

require('../../../css/ads.css');



const AdsPage = () => {

    const[currentPage, setCurrentPage] = useState(1);
    const[search, setSearch] = useState('');
    const [itemsPerPage, setItemsPerPage] = useState(12);
    const [loading, setLoading] = useState(true);
    const [ads, setAds] = useState([]);
    
    const [totalItems, setTotalItems] = useState(0);

    // Permet de récupérer toutes les annonces
    const fetchAds = async () => {       
        try{
            const data = await adsAPI.findPage(itemsPerPage, currentPage);
            const actualData = [...ads];
            const newData = actualData.concat(data);
            console.log(newData);
            setAds(newData);
            setLoading(false);
            setCurrentPage(currentPage + 1);
        }catch(error){
            console.log(error.response)
        }
        
    }

    // Au chargement du composant on va chercher les ads
    useEffect(() => {
        fetchAds();
        // fetchTotalItems();
    }, []);


    // const formatDate = (str) =>moment(str).format('DD/MM/YYYY');

    // //Gestion de la suppression d'une annonce
    // const handleDelete = async id => {
    //     const originalads = [...ads];
    //     setAds(ads.filter(ads => ads.id !== id));

    //     try{
    //         await adsAPI.delete(id);
    //     }catch(error){
    //         setads(originalads);
    //     }
    // };


    // Gestion de la recherche
    const handleSearch = ({currentTarget}) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
    }


    //Filtrage des customers en fonction de la recherche
    const filteredAds = ads.filter (
        a => 
            // a.price.toLowerCase().includes(search.toLowerCase()) ||
            a.title.toLowerCase().includes(search.toLowerCase()) ||
            a.brand.toLowerCase().includes(search.toLowerCase()) 
            // a.productType.toLowerCase().includes(search.toLowerCase())
    );

    // Pagination des données
    // const paginatedAds = Pagination.getData(filteredAds, currentPage, itemsPerPage);
   //TODO: Filtre à créer
    return (
        <>
            <title>PucesNautiques - les meilleurs canoës kayaks d'occasion près de chez vous</title>
            <div className="row">
                <div className="grey col-3">
                    <div className="form-group mt-5">
                        <h3>Vos recherches</h3>
                        <fieldset>
                            <div className="form-group">
                                <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher par titre ou marque ..."/>
                            </div>
                            <div className="form-group">
                                <div className="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" className="custom-control-input" checked="true"/>
                                    <label className="custom-control-label" for="customRadio1">Toggle this custom radio</label>
                                </div>
                                <div className="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio" className="custom-control-input"/>
                                    <label className="custom-control-label" for="customRadio2">Or toggle this other custom radio</label>
                                </div>
                                <div className="custom-control custom-radio">
                                    <input type="radio" id="customRadio3" name="customRadio" className="custom-control-input" disabled=""/>
                                    <label className="custom-control-label" for="customRadio3">Disabled custom radio</label>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="customCheck1" checked="true"/>
                                    <label className="custom-control-label" for="customCheck1">Check this custom checkbox</label>
                                </div>
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="customCheck2" disabled=""/>
                                    <label className="custom-control-label" for="customCheck2">Disabled custom checkbox</label>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="custom-control custom-switch">
                                    <input type="checkbox" className="custom-control-input" id="customSwitch1" checked="true"/>
                                    <label className="custom-control-label" for="customSwitch1">Toggle this switch element</label>
                                </div>
                                <div className="custom-control custom-switch">
                                    <input type="checkbox" className="custom-control-input" disabled="" id="customSwitch2"/>
                                    <label className="custom-control-label" for="customSwitch2">Disabled switch element</label>
                                </div>
                            </div>
                            <div className="form-group">
                                <select className="custom-select">
                                    <option selected="">Open this select menu</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div className="form-group">
                                <div className="input-group mb-3">
                                <div className="custom-file">
                                    <input type="file" className="custom-file-input" id="inputGroupFile02"/>
                                    <label className="custom-file-label" for="inputGroupFile02">Choose file</label>
                                </div>
                                <div className="input-group-append">
                                    <span className="input-group-text" id="">Upload</span>
                                </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset className="form-group">
                            <legend>Sliders</legend>
                            <label for="customRange1">Example range</label>
                            <input type="range" className="custom-range" id="customRange1"/>
                        </fieldset>
                    </div> 

                </div>
                <div className="col-9 pt-5">

                    <h1>Les annonces qui peuvent vous intéressez</h1>
                    {!loading && (
                        <InfiniteScroll
                        dataLength={ads.length}
                        next={fetchAds}
                        hasMore={true}
                        loader={<AdsLoader/>}>
                            <div className="ads-card-grid">
                                {filteredAds.map(ad => (
                                    <div key={ad.id} className="card border-light mb-3">
                                        <Link to={"/annonce/" + ad.id}>
                                            {ad.pictures[0] && ( 
                                                <img src={ad.pictures[0].path} />
                                            )}
                                        
                                            <div className="card-header">{ad.brand}</div>
                                            <div className="card-body">
                                                <h4 className="card-title">{ad.title}</h4>
                                                <p className="card-text">{ad.description.substring(0,50)}...</p>
                                            </div>
                                        </Link>
                                    </div>
                                ))}
                            </div>
                        </InfiniteScroll>
                        )} 
                        {loading && (<AdsLoader/>)}
                </div>
            </div>
            
            

            

        {/* <div classNameName="d-flex justify-content-between align-items-center">
            <h1>Liste des factures</h1>
            <Link classNameName="btn btn-info" to="/ads/new">Créer une facture</Link>
        </div>

        <div classNameName="form-group">
            <input type="text" onChange={handleSearch} value={search} classNameName="form-control" placeholder="Rechercher ..."/>
        </div> 

        <table classNameName="table table-hover">
            <thead>
                <tr>
                    <th>Numéro de facture</th>
                    <th>Client</th>
                    <th>Date</th>
                    <th classNameName="text-center" >Statut</th>
                    <th classNameName="text-center">Montant</th>
                    <th></th>
                </tr>
            </thead>
            {!loading && (
                <tbody>  
                {paginatedads.map(invoice => (
                    <tr key={invoice.id}>
                        <td><a href="#">{invoice.chrono}</a></td>
                        <td>
                            <Link to={"/customers/" + invoice.customer.id}>
                                {invoice.customer.firstName} {invoice.customer.lastName}
                            </Link>
                        </td>
                        <td>{formatDate(invoice.sentAt)}</td>
                        <td>
                            <span classNameName={"badge badge-" + STATUS_classNameES[invoice.status]}>{STATUS_LABELS[invoice.status]}</span> 
                        </td>
                        <td classNameName="text-center">{invoice.amount.toLocaleString()}</td>
                        <td>
                            <Link to={"/ads/" + invoice.id} classNameName="btn btn-sn btn-success mr-1">Editer</Link>
                            <button onClick={() => handleDelete(invoice.id)} classNameName="btn btn-sn btn-danger">Supprimer</button>
                        </td>
                    </tr>
                ))} 
            </tbody>
            )}
        </table>
        {loading && (<TableLoader/>)}

        {itemsPerPage < filteredads.length && 
            <Pagination 
                currentPage={currentPage} 
                itemsPerPage={itemsPerPage} 
                length={filteredads.length} 
                onPageChanged={handlePageChange}
            />
        } */}
    </>

    );
}
 
export default AdsPage;