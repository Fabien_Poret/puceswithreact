import React, { useEffect, useState } from 'react';
import adsAPI from '../../services/adsAPI';
import Pagination from '../../components/Pagination';
import moment from "moment";
import {Link} from "react-router-dom";
import AmazonLoader from '../../components/loaders/AmazonLoader';
import { toast } from 'react-toastify';

import Marker from '../../components/Marker';
import GoogleMapReact from 'google-map-react';

import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

const adViewPage = ({ match, history}) => {


    const {id} = match.params;

    const [ad, setAd] = useState({
        title: "",
        description: "",
        price: "",
        picture: "",
        brand: "",
        location: [],
        productType: [],
        category: [],
        user: [],
        createdAt: "",
    });

    const [loading, setLoading] = useState(false);
    const [product, setProduct] = useState([])

    const [latitude, setLatitude] = useState();
    const [longitude, setLongitude] = useState();

    const [picture, setPicture] = useState([]);

    const fetchAd = async id => { 
        //TODO : Faire attention à la CORS sur la mise en ligne du nouveau SRV

        try{
            // const data = await adsAPI.find(id);
            const {title, description, price, brand, location, productType, category, user, createdAt} = await adsAPI.find(id);
            setAd({title, description, price, brand, location, productType, category, user, createdAt});
            try {
                const pictures = await adsAPI.getPictures(id);
                setPicture(pictures);
            } catch (error) {
                toast.error("Les images n'ont pas pu être récupéré")
            }
            try{
                const adress =  await adsAPI.getMarker(location.city);
                const {lat, lon} = adress[0];
                setLatitude(lat);
                setLongitude(lon);
            }catch(error)
            {
                setLatitude(48.864716);
                setLongitude(2.349014);
            }

            setLoading(false);
            setProduct(productType.kayakrigide[0]);
        }catch(error){
            console.log(error.response)
            toast.error("L'annonce n'a pas pu être récupéré")
        }

    }


    
    // Au chargement du composant on va chercher les ads
    useEffect(() => {
        setLoading(true);
        fetchAd(id);
    }, [id])
    
    const formatDate = (str) =>moment(str).format('DD/MM/YYYY')

    return ( 
        <>
            <title>PucesNautiques - {ad.title}</title>
            <div className="container mt-5">
                {!loading && (
                    <>
                    <div className="row">
                        <div className="col-8">
                            <div className="row">
                                <h1>{ad.title}</h1>
                            </div>
                            <div className="row">
                                <h6>{ad.category.title}</h6>
                            </div>
                        </div>
                        <div className="col-4">
                            <div className="row">
                                <h3>Créé le {formatDate(ad.createdAt)}</h3>
                            </div>
                            <div className="row">
                            </div>
                            
                            
                        </div>
                    </div>
            
                    <hr/>

                    <div className="row">
                        <div className="col-5">
                            <Carousel arrows>                            
                                {picture && (
                                    picture.map(pic => 
                                    <>
                                        <img src={pic.path} alt={'pucesNautiques ' + id}/>
                                    </>
                                    )
                                )}
                            </Carousel>

                        </div>
                        <div className="col-7">
                            <div className="row">
                                <div className="col-9 pl-0">
                                    <p>Marque : {ad.brand}</p> 
                                </div>
                                <div className="col-3">
                                    {ad.price} €
                                </div>
                            </div>
                            <p>
                                <>
                                {ad.description}
                                </>
                            </p>
                        </div>
                    </div>

                    <hr/>

                    <div className="row">
                        <div className="col-6">
                            <div style={{ height: '50vh', width: '100%' }}>
                                <GoogleMapReact
                                    bootstrapURLKeys={{ key: "AIzaSyCr5kIfYpbi33ninkBbTBZ-W7Y3Icc8Wko" }}
                                    defaultZoom={7}
                                    defaultCenter={{lat:48.864716,lng:2.349014}}
                                    yesIWantToUseGoogleMapApiInternals
                                >
                                <Marker
                                    lat={latitude}
                                    lng={longitude}
                                    text="Mon produit d'occasion"
                                />
                                </GoogleMapReact>
                            </div>
                        </div>
                        <div className="col-6">
                        <p>{ad.user.firstName} {ad.user.lastName}</p>
                        {latitude}
                        {longitude}
                        </div>
                        {/* {ad.location.street}
                        {ad.productType.title}
                        {console.log(product)}
                        {console.log(ad.location.city)} */}
                        {/* {ad.productType.kayakrigide.[0].placeNumber} */}
                    </div>
     
                   
                </>

                )}
                {loading && 
                (
                    <>
                        <h1>Chargement de l'annonce</h1>
                        <AmazonLoader/>

                    </>
                )}

            </div>
        </>
     );
}
 
export default adViewPage;