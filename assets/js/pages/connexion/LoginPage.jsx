import React, {useState, useContext} from 'react';
import UsersAPI from '../../services/usersAPI';
import AuthContext from '../../contexts/AuthContext'
import Field from '../../components/forms/Fields';
import { toast } from 'react-toastify';
import Loader from '../../components/loaders/TreeDots';


const LoginPage = ({history }) => {

    const {setIsAuthenticated} = useContext(AuthContext);
    const [loading, setLoading] = useState(false);
    const[error, setError] = useState();

    const[credentials, setCredentials] = useState({
        username: "",
        password: ""
    });

    // Gestion des champs
    const handleChange = (event) => {
        const value = event.currentTarget.value;
        const name = event.currentTarget.name;
        setCredentials({...credentials, [name]: value});
    };
    
    // Gestion du submit
    const handleSubmit = async event => {
        event.preventDefault();
        try{
            setLoading(true)
            await UsersAPI.connexion(credentials)
            setError("");
            setIsAuthenticated(true);
            toast.success("Vous êtes désormais connecté !");
            history.replace("/");
        }catch(error){
            setLoading(false);
            setError("Aucun compte ne possède cette adresse email ou alors les informations ne correspondent pas");
            toast.error("Une erreur est survenue");
        }
    };

    return ( 
        <>
            <title>PucesNautiques - Connexion pour la vente de canoë kayak</title>
            <div className="container mt-5">
                <h1>Connexion à PucesNautiques</h1>
                {(!loading && (
                    <form onSubmit={handleSubmit}>
                        <Field 
                        label="Adresse email" 
                        name="username" 
                        value={credentials.username} 
                        onChange={handleChange} 
                        placeholder="Adresse email de connexion" 
                        error={error}
                        />
                        <Field 
                        label="Mot de passe" 
                        name="password" 
                        type="password"
                        value={credentials.password} 
                        onChange={handleChange} 
                        placeholder="Mot de passe" 
                        error={error}
                        />
                        <div className="form-group">
                            <button 
                                type="submit" 
                                className="btn btn-success">Connexion
                            </button>
                        </div>
                    </form>
                    ) || (
                    <>
                        <Loader />
                    </>
                ))}
            
            </div>           
        </>
    );
}
   
export default LoginPage;