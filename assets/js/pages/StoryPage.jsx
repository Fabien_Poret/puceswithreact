import React from 'react';

const StoryPage = () => {
    return ( 
        <>
            <title>PucesNautiques - L'histoire des amoureux du sport nautique</title>
            <div className="container mt-5">
                <h1>L'histoire des puces nautiques</h1>
                <h2>Heading 2</h2>
                <h3>Heading 3</h3>
                <h4>Heading 4</h4>
                <h5>Heading 5</h5>
                <h6>Heading 6</h6>
                <h3>
                    Heading
                    <small class="text-muted">with muted text</small>
                </h3>
                <p class="lead">
                    Pellentesque venenatis taciti tristique architecto consequuntur quam magnis labore ab imperdiet semper modi se
                    mper risus! Molestiae sed sodales interdum vehicula. Duis sodales porro vivamus lobortis incididunt fringilla quia. M
                    inima tempor urna saepe maecenas possimus elit architecto inceptos lectus animi pariatur explicabo, hac? Fugit phasellus
                    dapibus ipsam in leo anim nostrud. Et dolor impedit aenean adipisicing, taciti, ipsam phasellus cumque officiis! Peat
                    nemo perspiciatis vestibulum inceptos ullamco, ea massa temporibus minim, pretium accumsan? Beatae soluta potenti,
                    xcepturi varius natus fermentum vulputate? Pellentesque rerum proin, nisi,
                    sed totam distinctio illum deleniti! Fusce maxime unde bibendum sit porro, eiusmod nascetur, arcu totam.
                    Quia culpa adipiscing, turpis rhoncus sapien vulputate massa vero lorem totam facere? Rerum cillum, consectetuer vol
                    uptatibus, voluptate, faucibus? Minima, dui at, corrupti? Nascetur asperiores. Eu facilisi dignissim mus et unde. Numq
                    uam nunc facilisis wisi blandit sit mi nam, semper scelerisque minima porro? Pharetra! Nostrum sapien. Accusantium. Enim 
                    pulvinar? Expedita dicta condimentum euismod varius volutpat? Excepturi aut, recusandae officiis at voluptatem lacus 
                    lacinia inventore? Tincidunt deserunt cras delectus ridiculus exercitationem nascetur commodi esse? Risus sequi possimus,
                    officia suspendisse ipsam, hymenaeos explicabo suscipit? At aute mattis sem ipsa, suspendisse! Cupidatat omnis sint do 
                    accusamus doloremque quos perferendis ea ex purus ratione gravida.
                    Nulla mollis nemo vel corporis natoque laborum sociosqu? Magni facilisis dignissimos aptent, donec a ullam. Assumend
                    a minus inceptos? Aliqua blanditiis, feugiat neque, senectus dolore repudiandae odit cupidatat voluptatum, feugiat vo
                    lutpat, odit ullamcorper, eget, neque irure hymenaeos quo euismod sollicitudin soluta senectus. Illum, aliquet hac sunt 
                    praesentium quae suscipit! At proin, quaerat eget perferendis tellus ullam, sagittis, tempore molestiae cillum quod, co
                    ndimentum tortor do eveniet numquam accusamus! Nostra aspernatur, eget atque. Rutrum vero quam taciti est minima error
                    sem ut dicta, accusantium nostrud soluta nobis curae consequat magnis. Ante rutrum, aspernatur, iaculis ornare! Erat 
                    expedita ex dicta habitant quibusdam alias do.
                </p>
            </div>
        </>
     );
}
 
export default StoryPage;