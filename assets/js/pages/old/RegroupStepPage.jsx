import React, {useState} from 'react';
import Step1_adminPage from './Step1_adminPage';
import Step2_adminPage from './Step2_adminPage';
import Step3_adminPage from './Step3_adminPage';
import Step4_adminPage from './Step4_adminPage';

const adForm = () => {
    
    const [currentStep, setCurrentStep] = useState(1);


    const prev = () => {
        if (currentStep !== 1) {
            setCurrentStep(currentStep - 1); 
        }

    }

    const next = () => {
        if (currentStep !== 4) {
            setCurrentStep(currentStep + 1);
        }
   
    }

    return (  
        <>
        <h1>Formulaire de création d'annonce</h1>


        {currentStep == 1 && (
            <Step1_adminPage/>
        )}
         {currentStep == 2 && (
            <Step2_adminPage/>
        )}
        {currentStep == 3 && (
            <Step3_adminPage/>
        )}
        {currentStep == 4 && (
            <Step4_adminPage/>
        )}



        <button 
            className="btn btn-primary" 
            onClick={prev}
            >
            prev
        </button>       

        <button 
            className="btn btn-primary" 
            onClick={next}
            >
            Next
        </button>    
        </>

    );
}
 
export default adForm;