import React, {useState, useEffect } from 'react';
import Field from '../../components/forms/Fields';
import FormContentLoader from '../../components/loaders/FormContentLoader';
import { Link } from 'react-router-dom';
import usersAPI from '../../services/usersAPI';
import { toast } from 'react-toastify';
import adsAPI from '../../services/adsAPI';
import picturesAPI from '../../services/picturesAPI';
import categoriesAPI from '../../services/categoriesAPI';

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import Select from '../../components/forms/Select';
import Picture from '../../components/forms/Picture';
import ImageUploader from 'react-images-upload';

import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';


const step1 = ({ match, history}) => {
    
    // const {id} = match.params;
    
    const [loading, setLoading] = useState(false);

    const [editing, setEditing] = useState(false);

    const [ad, setAd] = useState({
        title: "",
        description: "",
        price: "",
        picture: "",
        brand: "",
        // location: "",
        // productType: "",
        category: "",
        // user: "",
        createdAt: "",
    });

    const[error, setError] = useState({
        title: "",
        description: "",
        price: "",
        picture: "",
        brand: "",
        location: "",
        productType: "",
        category: "",
        user: "",
        createdAt: "",
    });



    // Création d'un select pour la catégorie 
    // Récupération de l'utilisateur connecté 

    //Récupération d'une seule ad
    const fetchAd = async id => {
        try {
            console.log(id);
            const {title, description, price, picture, brand, category} = await adsAPI.find(id);
    
            setLoading(false);
            setAd({title, description, price, picture, brand, category: "/api/categories/" + category.id});

        } catch (error) {
            console.log(error.response);
            toast.error("L'annonce n'a pas pu être récupéré")
        }
    }


    //Chargement ad si besoin au chargement du composant ou au changement de l'identifiant
    useEffect(() => {

        if(id !== "new"){
            setLoading(true);
            setEditing(true);
            fetchAd(id);          
        }else{
            setAd({title: "Annonce.."});
        }
    }, [id])

    // useEffect(() => {
    // }, [loading == false])
  
    //Gestion des changements des inputs dans le formulaire
    const handleChange = ({ currentTarget}) => {
        const {name, value} = currentTarget;
        setAd({ ...ad, [name]: value });
        console.log(ad);

    };


    // Gestion de la soumission du formulaire
    const handleSubmit = async event => {
        //preventDefault permet de ne pas recharger la page
        event.preventDefault();
        console.log(ad);
        setLoading(true);
        try{
            if(editing){
                console.log(id);
                const response = await adsAPI.update(id, ad);
                console.log(response);
                toast.success("Le formulaire a bien été pris en compte");
                history.replace("/administration/");

            }
            else {
                const response = await adsAPI.create(ad);
                console.log(response);
                toast.success("Le formulaire a bien été pris en compte");
                history.replace("/administration/");
            }
            // try {
            //     if(pictures){
            //         const response = await picturesAPI.postPicture(picture);
            //     }
            // } catch (error) {
                
            // }
        }catch({response}){

            console.log(response);
            const {violations} = response.data;

            setLoading(false);

            if(violations){
                const apiErrors = {};
                violations.forEach(({ propertyPath, message}) => {
                    apiErrors[propertyPath] = message;
                });
                setError(apiErrors);
            }
            toast.error("Une erreur est survenue");
        }
    };

    return ( 
        <>
           <form onSubmit={handleSubmit}>
                 
                 <Field name="title" label="Titre de l'annonce" error={error.title} placeholder="Titre de l'annonce..." value={ad.title} onChange={handleChange}/>
                <div className="row">
                    <div className="col-6 pl-0">
                         <Field name="price" type="number" label="Prix de l'annonce en €" error={error.price} placeholder="Prix de l'annonce..." value={ad.price} onChange={handleChange} />
                    </div>
                    <div className="col-6 pr-0">
                         <Field name="brand"  label="Marque du produit" error={error.brand} placeholder="Marque du produit..." value={ad.brand} onChange={handleChange} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 pl-0">
                         <Select 
                             name="category" 
                             label="Catégorie de l'annonce"
                             value={ad.category}
                             error={error.category}
                             onChange={handleChange}
                         >
                             <option key="0" value="0">...
                             </option>  
                             {categories.map(category => 
                                 <option key={category.id} value={"/api/categories/" + category.id}>{category.title}
                                 </option>  
                             )}
                         </Select>
                    </div>
                 </div>
                 {ad.title && (
                    <CKEditor
                    editor={ ClassicEditor }
                    data={!editing ? ("..") : (ad.description)}
                    onInit={ editor => {
                    } }
                    onChange={ ( event, editor ) => {
                        const data = editor.getData();
                        console.log(data);
                        setAd({...ad, description: data});
                    } }
                    onBlur={ ( event, editor ) => {
                        console.log( 'Blur.', editor );
                    } }
                    onFocus={ ( event, editor ) => {
                        console.log( 'Focus.', editor );
                    } }
                />
                )}
            </form>   
        </>
     );
}
 
export default step1;