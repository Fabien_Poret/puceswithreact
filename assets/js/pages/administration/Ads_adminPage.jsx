import React, { useEffect, useState } from 'react';
import Pagination from '../../components/Pagination';
import adsAPI from "../../services/adsAPI";
import TableLoader from "../../components/loaders/TableLoader"
import { Link } from 'react-router-dom';
import InfiniteScroll from "react-infinite-scroll-component";
import moment from "moment";
import usersAPI from "../../services/usersAPI";

const Ads_AdminPage = (props) => {

    const[ads, setAds] = useState([]);
    const[currentPage, setCurrentPage] = useState(1);
    const[search, setSearch] = useState('');
    const itemsPerPage = 15;
    const[loading, setLoading] = useState(true);


    // Permet de récupérer les ads 
    const fetchAds = async () => {       
        try{
            const data = await usersAPI.getAdsByUser();

            console.log(data);
            setAds(data);
            setLoading(false);
            setCurrentPage(currentPage + 1);
        }catch(error){
            console.log(error.response)
        }
        
    }

    // Au chargement du composant on va chercher les ads
    useEffect(() => {
        fetchAds();
    }, []);


    //Gestion de la suppression d'un customer
    const handleDelete = async id => {
        const originalAds = [...ads];
        setAds(ads.filter(ad => ad.id !== id));

        try{
            await adsAPI.delete(id);
        }catch(error){
            setAds(originalAds);
        }
    };

    // //Gestion du changement de page
    const handlePageChange = page => {
        setCurrentPage(page);
        
    }

    // Gestion de la recherche
    const handleSearch = ({currentTarget}) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
    }


    //Filtrage des ads en fonction de la recherche
    const filteredAds = ads.filter (
        a => 
            // a.Price.toLowerCase().includes(search.toLowerCase()) ||
            a.title.toLowerCase().includes(search.toLowerCase()) ||
            a.brand.toLowerCase().includes(search.toLowerCase())
            // a.productType.toLowerCase().includes(search.toLowerCase())
    );

    const formatDate = (str) =>moment(str).format('DD/MM/YYYY');
    // const filteredAds = [...ads];
    // Pagination des données
    // const paginatedAds = Pagination.getData(filteredAds, currentPage, itemsPerPage);
    
    //TODO: Pagination à créer
    return ( 
        <>
            <title>PucesNautiques - Vos annonces de vente de canoë ou kayak</title>
            <div className="container">
                <div className="mb-3 mt-3 d-flex justify-content-between align-items-center">
                    <h1>Liste de mes annonces</h1>
                    <Link to='/annonce/formulaire/new' className="btn btn-info">Créer une annonce</Link>
                </div>

                <div className="form-group">
                    <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher par titre ou marque ..."/>
                </div> 

                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Titre</th>
                            <th>Créé le</th>
                            <th>Catégorie</th>
                            <th>Marque</th>
                            <th className="text-center" >Prix</th>
                            <th></th>
                        </tr>
                    </thead>

                    {!loading && (
                        <tbody>                        
                            {ads.map(ad => (
                                <tr key={ad.id}>
                                    <td>
                                        <Link to={"/annonce/" + ad.id}>
                                            {ad.id}
                                        </Link>
                                    </td>
                                    <td>
                                        <Link to={"/annonce/" + ad.id}>
                                            {ad.title}
                                        </Link>
                                    </td>
                                    <td>
                                        {formatDate(ad.createdAt)}
                                    </td>
                                    <td>{ad.category.title}</td>
                                    <td>{ad.brand}</td>
                                    <td className="text-center"><span className="badge badge-primary">{ad.price}</span></td>
                                    <td>
                                        <Link to={"/annonce/formulaire/" + ad.id}>
                                            <button type="button" className="btn btn-primary">Edition</button>
                                        </Link>
                                        <button onClick={() => handleDelete(ad.id)} className="btn btn-sn btn-danger">Supprimer</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>

                    )} 

                </table>
                {loading && (<TableLoader/>)}

                {/* {itemsPerPage < filteredAds.length && 
                    <Pagination 
                        currentPage={currentPage} 
                        itemsPerPage={itemsPerPage} 
                        length={filteredAds.length} 
                        onPageChanged={handlePageChange}
                    />
                } */}

            </div>
        </>
     ); 
};

export default Ads_AdminPage;