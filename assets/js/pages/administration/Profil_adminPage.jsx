import React, { useEffect, useState } from 'react';
import usersAPI from '../../services/usersAPI';
import FormContentLoader from '../../components/loaders/FormContentLoader';
import Field from '../../components/forms/Fields';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import Select from '../../components/forms/Select';

const Profil_adminPage = ({ match, history}) => {

    const [user, setUser] = useState([]);
    const [errors, setErrors] = useState([]);
    const [loading, setLoading] = useState(true);
    // const [locations, setLocations] = useState([]);
    // const [defaultAdress, setDefaultAdress] = useState({
    //     id : "",
    //     cp: "",
    //     city: "",
    //     street: "",
    //     streetNumber: "",
    // });

    const fetchUser = async() => {
        try {
            const user = await usersAPI.getUser();
            // const location = await usersAPI.getLocationByUser();

            setUser(user);
            // setLocations(location);
            setLoading(false);

            // const adress = location.filter(location => location.defaultAdress == true);
            // const {id, cp, city, street, streetNumber} = adress[0];
            // setDefaultAdress({id, cp, city, street, streetNumber});

        } catch (error) {
            console.log(error.response);
            toast.error("L'utilisateur n'a pas pu être récupéré")
        }
    }

 
    //Gestion des changements des inputs dans le formulaire
    const handleChange = ({ currentTarget}) => {
        const {name, value} = currentTarget;
        setUser({ ...user, [name]: value });
        console.log(user);
    };

    // Gestion de la soumission du formulaire
    const handleSubmit = async event => {
        //preventDefault permet de ne pas recharger la page
        event.preventDefault();
        console.log(user);
        setLoading(true);

        try{
            console.log(response);
            const response = await usersAPI.update(user.id, user);
            toast.success("Le formulaire a bien été pris en compte");
            history.replace("/administration/");
        }
        catch({response}){
            console.log(response);
            const {violations} = response.data;
            setLoading(false);
            if(violations){
                const apiErrors = {};
                violations.forEach(({ propertyPath, message}) => {
                    apiErrors[propertyPath] = message;
                });
                setErrors(apiErrors);
                toast.error("Une erreur est survenue");
            }
        }
    };
    
    //Chargement ad si besoin au chargement du composant ou au changement de l'identifiant
    useEffect(() => {
        fetchUser()
    }, [])

    //TODO : Changer le mot de passe 
    //TODO : Changer l'adresse
    return ( 
        <>
            <title>PucesNautique - Modification du compte</title>
            <div className="container mt-5">
                <h1>Modification du compte {user.email}</h1>
                {!loading && (
                <>
                    <form onSubmit={handleSubmit}>
                        <Field
                            name="firstName"
                            label="Prénom*"
                            placeholder="Votre joli prénom"
                            error={errors.firstName}
                            value={user.firstName}
                            onChange={handleChange}
                        />
                        <Field
                            name="lastName"
                            label="Nom de famille*"
                            placeholder="Votre nom de famille"
                            error={errors.lastName}
                            value={user.lastName}
                            onChange={handleChange}
                        />
                        <Field
                            name="email"
                            label="Email*"
                            placeholder="Votre adresse email"
                            error={errors.email}
                            value={user.email}
                            onChange={handleChange}
                        />

                        {/* <div className="row">
                            <div className="col-8 pl-0">
                                <Select 
                                    name="location" 
                                    label="Mes adresses"
                                    value={location.id}
                                    onChange={handleChange}
                                >
                      
                                    <option selected key={defaultAdress.id} value={"/api/location/" + defaultAdress.id}>
                                        {defaultAdress.streetNumber} - {defaultAdress.street} - {defaultAdress.cp} - {defaultAdress.city}   
                                    </option>

                                    {locations.map(location  => 
                                        <option key={location.id} value={"/api/location/" + location.id}>
                                            {location.streetNumber} - {location.street} - {location.cp} - {location.city}   
                                        </option>
                                    )}
                                </Select>
                            </div>
                            <div className="col-4 pr-0">
                                <Link to="/" className="btn btn-link">Ajouter une adresse</Link>
                            </div>
                        </div> */}

                        <div className="form-group">
                            <button 
                                type="submit" 
                                className="btn btn-success">Enregistrer les modifications
                            </button>
                            <Link to="/administration" className="btn btn-link">Je ne souhaite pas modifier mon compte</Link>
                        </div>
                    </form>
                </>
                )}
                {loading && (<FormContentLoader/>)}
            </div>
        </>
     );
}
 
export default Profil_adminPage;