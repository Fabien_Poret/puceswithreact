import React, {useState, useEffect } from 'react';
import Field from '../../../components/forms/Fields';
import FormContentLoader from '../../../components/loaders/FormContentLoader';
import { Link } from 'react-router-dom';
import usersAPI from '../../../services/usersAPI';
import { toast } from 'react-toastify';
import adsAPI from '../../../services/adsAPI';
import picturesAPI from '../../../services/picturesAPI';
import categoriesAPI from '../../../services/categoriesAPI';

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import Select from '../../../components/forms/Select';
import ProgressBar from '../../../components/ProgressBar';
import ImageUploader from 'react-images-upload';

import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';


const Ads_formPage = ({ match, history}) => {

    const {id} = match.params;
    
    const [loading, setLoading] = useState(false);
    const [product, setProduct] = useState([])

    const [editing, setEditing] = useState(false);

    const [ad, setAd] = useState({
        title: "",
        description: "",
        price: "",
        picture: "",
        brand: "",
        // location: "",
        // productType: "",
        category: "",
        // user: "",
        createdAt: "",
    });
    const [categories, setCategories] = useState([]);

    const[error, setError] = useState({
        title: "",
        description: "",
        price: "",
        picture: "",
        brand: "",
        location: "",
        productType: "",
        category: "",
        user: "",
        createdAt: "",
    });

    const [locations, setLocations] = useState([]);
    const [pictures, setPictures] = useState({
        ad: '',
        path: ''
    });

    const [path, setPath] = useState([]);

    // Création d'un select pour la catégorie 
    // Récupération de l'utilisateur connecté 

    //Récupération d'une seule ad
    const fetchAd = async id => {
        try {
            console.log(id);
            const {title, description, price, picture, brand, category} = await adsAPI.find(id);
    
            setLoading(false);
            setAd({title, description, price, picture, brand, category: "/api/categories/" + category.id});

        } catch (error) {
            console.log(error.response);
            toast.error("L'annonce n'a pas pu être récupéré")
        }
    }

    //Récupération des catégories
    const fetchCategory = async () => {
        try {
            const data = await categoriesAPI.findAll();
            setCategories(data);
            setLoading(false);

        } catch (error) {
            console.log(error.response);
            toast.error("La catégorie n'a pas pu être récupéré")
        }
    }

    const fetchAdress = async () => {
        console.log('test');
        try {
            const location = await usersAPI.getLocationByUser();
            console.log(location);
            setLocations(location);
        } catch (error) {
            console.log(error.response);
            toast.error("Les adresses n'ont pas pu être récupéré")
        }
    }
    //Chargement ad si besoin au chargement du composant ou au changement de l'identifiant
    useEffect(() => {
        fetchCategory();
        fetchAdress();
        if(id !== "new"){
            setLoading(true);
            setEditing(true);
            fetchAd(id);          
        }else{
            setAd({title: "Annonce.."});
        }
    }, [id])

    // useEffect(() => {
    // }, [loading == false])
  
    //Gestion des changements des inputs dans le formulaire
    const handleChange = ({ currentTarget}) => {
        const {name, value} = currentTarget;
        setAd({ ...ad, [name]: value });
        console.log(ad);
    };

    const onDrop = (pictureFiles, pictureDataURLs) =>{
        console.log(pictureDataURLs);
        console.log(pictureFiles);
        setPath(pictureDataURLs);
        // setPictures({[ad]: ad, [path]: [pictureDataURLs]});
        // setPictures({[ad]: ad, [path]: path})
    };

    // Gestion de la soumission du formulaire
    const handleSubmit = async event => {
        //preventDefault permet de ne pas recharger la page
        event.preventDefault();
        console.log(ad);
        setLoading(true);
        try{
            if(editing){
                console.log(id);
                const response = await adsAPI.update(id, ad);
                console.log(response);
                toast.success("Le formulaire a bien été pris en compte");
                history.replace("/annonce/caracteristique/" + response.id + "/edition");

            }
            else {
                const response = await adsAPI.create(ad);
                console.log(response.id);
                toast.success("Le formulaire a bien été pris en compte");
                history.replace("/annonce/caracteristique/" + response.id + "/création");
            }
            // try {
            //     if(pictures){
            //         const response = await picturesAPI.postPicture(picture);
            //     }
            // } catch (error) {
                
            // }
        }catch({response}){

            console.log(response);
            const {violations} = response.data;

            setLoading(false);

            if(violations){
                const apiErrors = {};
                violations.forEach(({ propertyPath, message}) => {
                    apiErrors[propertyPath] = message;
                });
                setError(apiErrors);
            }
            toast.error("Une erreur est survenue");
        }
    };
    
        
    // const formatDate = (str) =>moment(str).format('DD/MM/YYYY');

    // TODO: Ajouter les champs pour les catégories, l'adresse, et les caractéristiques du bateau
    // TODO: Créer un événement pour la création ou modification d'une annonce et ajouter automatiquement un nouveau datetime et user
    // TODO : Créer des conditions de test
    return (    
    <>
        <title>PucesNautiques - Créer votre annonce de vente de canoë ou kayak</title>
        <div className="container mt-5">
            
            <ProgressBar currentStep={1}/>
            {!editing &&  <h1>Création d'une annonce</h1> || <h1>Modification d'une annonce</h1>}
        

            {loading && (<FormContentLoader/>)}
            
            
            {!loading && (
             <form onSubmit={handleSubmit}>
                 
                <Field name="title" label="Titre de l'annonce" error={error.title} placeholder="Titre de l'annonce..." value={ad.title} onChange={handleChange}/>
               <div className="row">
                   <div className="col-6 pl-0">
                        <Field name="price" type="number" label="Prix en €" error={error.price} placeholder="Prix de l'annonce..." value={ad.price} onChange={handleChange} />
                   </div>
                   <div className="col-6 pr-0">
                        <Field name="brand"  label="Marque du produit" error={error.brand} placeholder="Marque du produit..." value={ad.brand} onChange={handleChange} />
                   </div>
               </div>
               <div className="row">
                   <div className="col-12 pl-0">
                        <Select 
                            name="category" 
                            label="Catégorie de l'annonce"
                            value={ad.category}
                            error={error.category}
                            onChange={handleChange}
                        >
                            <option key="0" value="0">...
                            </option>  
                            {categories.map(category => 
                                <option key={category.id} value={"/api/categories/" + category.id}>{category.title}
                                </option>  
                            )}
                        </Select>
                   </div>
                   <div className="col-6 pr-0">
                        <div className="row">
                            <div className="col-8 pl-0">
                                {/* <Select 
                                    name="location" 
                                    label="L'adresse de la vente"
                                    value={ad.location}
                                    onChange={handleChange}
                                >
                                <option key="0" value="0">...
                                </option>  
                                    {locations.map(location  => 
                                        <option key={location.id} value={"/api/locations/" + location.id}>
                                            {location.streetNumber} - {location.street} - {location.cp} - {location.city}   
                                        </option>
                                    )}
                                </Select> */}
                            </div>
                            <div className="col-4 pr-0">
                                {/* <Link to="/administration/adresse" className="btn btn-link">Nouvelle adresse</Link> */}
                            </div>
                        </div>
                   </div>
                </div>

                <div className="row">
                    <div className="col-6 pl-0">
                        {/* <label>
                            Mes images téléchargées 
                        </label>
                        <Carousel
                            animationSpeed={1500}
                            autoPlay={3000}
                            stopAutoPlayOnHover
                            clickToChange
                        >                         
                            {path && (
                                path.map(pic => 
                                <>
                                    <img style={{ height: '10vh', width: '50%' }} src={pic} alt={pic}/>
                                </>
                                )
                            )} 
                        </Carousel> */}
                    </div>
                    <div className="col-6 pl-0">
                        {/* <ImageUploader
                                withIcon={true}
                                buttonText='Choisir une image'
                                onChange={onDrop}
                                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                maxFileSize={5242880}
                            /> */}
                    </div>
                </div>
            
               {/* <Field name="picture" label="Images de l'annonce..." error={error.picture} placeholder="Images de l'annonce..." value={ad.picture} onChange={handleChange} /> */}
                {ad.title && (
                    <CKEditor
                    editor={ ClassicEditor }
                    data={!editing ? ("..") : (ad.description)}
                    onInit={ editor => {
                    } }
                    onChange={ ( event, editor ) => {
                        const data = editor.getData();
                        console.log(data);
                        setAd({...ad, description: data});
                    } }
                    onBlur={ ( event, editor ) => {
                        console.log( 'Blur.', editor );
                    } }
                    onFocus={ ( event, editor ) => {
                        console.log( 'Focus.', editor );
                    } }
                />
                )}
                
                <div className="form-groupn mt-5">
                    <button type="submit" className="btn btn-success">
                        Enregistrer
                    </button>
                    <Link to="/administration" className="btn btn-link">Retour à la liste</Link>
                </div> 
            </form>
            )} 
        </div>

    </>
     );
}
 
export default Ads_formPage;