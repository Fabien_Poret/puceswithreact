import React, {useState, useEffect} from 'react';

import ProgressBar from '../../../components/ProgressBar';

// import ImageUploader from 'react-images-upload';
import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

import FormContentLoader from '../../../components/loaders/FormContentLoader';
import { Link } from 'react-router-dom';

import picturesAPI from '../../../services/picturesAPI';
import axios, { post } from 'axios';

import { toast } from 'react-toastify';

const Pictures_formPage = ({ match, history}) => {

    const {id, state} = match.params;

    const [loading, setLoading] = useState(true);
    const [paths, setPaths] = useState([]);
    const [pictures, setPictures] = useState();
    const [pictureFiles, setPictureFiles] = useState();
    const [editing, setEditing] = useState(false);

    useEffect(() => {
        if(state == 'edition')
        {
            setEditing(true);
            fetchPictures();
        } else {
            setLoading(false);
        }
        // 
    }, [id])

    const fetchPictures = async () => {
        console.log(id);
        try {
            const data = await picturesAPI.findByAd(id);
            setPictures(data);
            setLoading(false);

        } catch (error) {
            console.log(error.response);
            toast.error("Les images existantes n'ont pas pu être récupéré");
            setPictures();
            setLoading(false);

        }
    }

    const onDrop = (pictureFiles, pictureDataURLs) =>{
        console.log(pictureDataURLs);
        console.log(pictureFiles);
        setPaths(pictureDataURLs);
        setPictureFiles(pictureFiles);

        // setPictures([...pictures, { ad: "/api/ads/" + id, path: pictureFiles}]);
        // pictureFiles.map(pictureFile => {

        //     ;

            
        // });
        // {pictureFiles.flatMap(pictureFile => {
        //     setPictures([...pictures] , { ad: "/api/ads/" + id, path: pictureFile.name});
        //     console.log(pictures);
        // })}

    };

    // const fileUpload = (file) => {
    //     const url = 'D:/Dev/pucesNautiques/pucesReact/pucesnautiqueReact/public/images';
    //     const formData = new FormData();
    //     formData.append('file',file)
    //     const config = {
    //         headers: {
    //             'content-type': 'multipart/form-data'
    //         }
    //     }
    //     return post(url, formData,config)
    // }

    const handleSubmit = async event => {
    //     //preventDefault permet de ne pas recharger la page
        event.preventDefault();
        console.log(paths);
    //     console.log(ad);
        // setLoading(true);
        // pictureFiles.forEach(pictureFile => {
        //     fileUpload(pictureFile);
        // });
        try{
            toast.success("Le formulaire a bien été pris en compte");
            history.replace("/annonce/adresse/" + id +  "/" + state);

            // paths.map(path => {
            //     setPictures({ad: "/api/ads/" + id, path});
            //     const response = await picturesAPI.postPicture(pictures)
            //     console.log(pictures);
            // })

    //         if(editing){
    //             console.log(id);
    //             const response = await adsAPI.update(id, ad);
    //             console.log(response);
    //             toast.success("Le formulaire a bien été pris en compte");
    //             history.replace("/annonce/images/" + response.id);

    //         }
    //         else {
    //             const response = await adsAPI.create(ad);
    //             console.log(response.id);
    //             toast.success("Le formulaire a bien été pris en compte");
    //             history.replace("/annonce/images/" + response.id);
    //         }
    //         // try {
    //         //     if(pictures){
    //         //         const response = await picturesAPI.postPicture(picture);
    //         //     }
    //         // } catch (error) {
                
    //         // }
        }catch({response}){

    //         console.log(response);
    //         const {violations} = response.data;

    //         setLoading(false);

    //         if(violations){
    //             const apiErrors = {};
    //             violations.forEach(({ propertyPath, message}) => {
    //                 apiErrors[propertyPath] = message;
    //             });
    //             setError(apiErrors);
    //         }
            toast.error("Une erreur est survenue");
        }
    };

    //TODO:  Formulaire pour les images à créer

    return ( 
        <>
            <title>PucesNautiques - Créer votre annonce de vente de canoë ou kayak</title>
            <div className="container mt-5">

                <ProgressBar currentStep={3}/>

                <h1>Les images de votre annonce</h1>

                {loading && (<FormContentLoader/>)}
                {!loading && (
                    <form onSubmit={handleSubmit}>
                        <div className="row">
                            <div className="col-6 pl-0">
                                <label>
                                    Mes images téléchargées 
                                </label>
                                {/* <Carousel
                                    animationSpeed={1500}
                                    autoPlay={3000}
                                    stopAutoPlayOnHover
                                    clickToChange
                                >                         
                                    {paths && (
                                        paths.map(pic => 
                                        <>
                                            <img style={{ height: '10vh', width: '50%' }} src={pic} alt={pic}/>
                                        </>
                                        )
                                    )} 
                                </Carousel> */}
                            </div>
                            <div className="col-6 pl-0">
                                {/* <ImageUploader
                                        withIcon={true}
                                        buttonText='Choisir une image'
                                        onChange={onDrop}
                                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                        maxFileSize={5242880}
                                        label='5mo maximum'
                                    /> */}
                            </div>
                        </div>
                        <div className="form-groupn mt-5">
                            <button type="submit" className="btn btn-success">
                                Enregistrer
                            </button>
                            <Link to="/administration" className="btn btn-link">Retour à la liste</Link>
                        </div> 
                    </form>
                )}
            </div>
            
        </>
     );
}
 
export default Pictures_formPage;