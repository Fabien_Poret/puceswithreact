import React, {useState, useEffect} from 'react';
import ProgressBar from '../../../components/ProgressBar';
import FormContentLoader from '../../../components/loaders/FormContentLoader';
import Field from '../../../components/forms/Fields';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';

const Caract_FormPage = ({ match, history}) => {
    
    const {uri, state} = match.params;
    const [produit, setProduit] = useState();
    const [loading, setLoading] = useState(true);
    const [editing, setEditing] = useState(false);

    useEffect(() => {
        if(state == 'edition')
        {
            setEditing(true);
            // fetchAdress();
            // fetchPictures();
        } else {
            setLoading(false);
        }
    }, [uri])
    

    // const fetchCaract = async() => {
    //     try {
    //         const {id, cp, city, street, streetNumber, country} = await locationAPI.fetchLocationByAd(uri);
    //         setLocation({id, cp, city, street, streetNumber, country});
    //         setLoading(false);

    //     } catch (error) {
    //         console.log(error.response);
    //         toast.error("L'adresse n'a pas pu être récupéré")
    //     }
    // }

    
 
    //Gestion des changements des inputs dans le formulaire
    const handleChange = ({ currentTarget}) => {

        const {name, value, type} = currentTarget;
        console.log({name, value, type});

        if(type == "number")
        {
            // const number = parseInt(value);
            // console.log(number);
            // setLocation({ ...location, [name]: number });
        }else {
            // setLocation({ ...location, [name]: value });
        }
        // setLocation({ ...location, [user]: ""});
    };

    // Gestion de la soumission du formulaire
    const handleSubmit = async event => {
        //preventDefault permet de ne pas recharger la page
        // event.preventDefault();
        // console.log(location);
        // setLoading(true);

        
        try{

            // if(editing){
                // console.log(location.id);
                // const putLocation = await locationAPI.update(location.id, location);
                // console.log(putLocation);
            // }
            // else {
                // const postLocation = await locationAPI.post(location);
                // const updateAd = await adsAPI.update(id, {location: "/api/locations/" + postLocation.id});
            // }

            toast.success("Le formulaire a bien été pris en compte");
            history.replace("/annonce/images/" + uri +  "/" + state);
        }
        catch({response}){
            console.log(response);
            // const {violations} = response.data;
            // setLoading(false);
            // if(violations){
            //     const apiErrors = {};
            //     violations.forEach(({ propertyPath, message}) => {
            //         apiErrors[propertyPath] = message;
            //     });
            //     setErrors(apiErrors);
            // }
            toast.error("Une erreur est survenue");
        }
    };
    

    //TODO: Créer le formulaire 
    //TODO: Gérer le conflit avec le type de produit (productType)


    return ( 
        <>
            <title>PucesNautiques - Créer votre annonce de vente de canoë ou kayak</title>
            <div className="container mt-5">
                <ProgressBar currentStep={2}/>
                {!editing &&  <h1>Caractéristique du produit</h1> || <h1>Mise à jour du produit</h1>}
                {loading && (<FormContentLoader/>)}
                {!loading && (
                    <form onSubmit={handleSubmit}>
                        <div className="row">

                        </div>
                        <div className="form-groupn mt-5">
                            <button type="submit" className="btn btn-success">
                                Enregistrer
                            </button>
                            <Link to="/administration" className="btn btn-link">Retour à la liste</Link>
                        </div> 
                    </form>
                )}
            </div>
            
        </>
     );
}
 
export default Caract_FormPage;