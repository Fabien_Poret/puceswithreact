import React, { useEffect, useState } from 'react';
import locationAPI from '../../../services/locationAPI';
import adsAPI from '../../../services/adsAPI';
import FormContentLoader from '../../../components/loaders/FormContentLoader';
import Field from '../../../components/forms/Fields';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import Select from '../../../components/forms/Select';
import ProgressBar from '../../../components/ProgressBar';


const Profil_adminPage = ({ match, history}) => {

    const {uri, state} = match.params;
    const [errors, setErrors] = useState([]);
    const [loading, setLoading] = useState(true);
    const [location, setLocation] = useState({
        id: '',
        cp: '',
        city: '',
        street: '',
        streetNumber: '',
        country: ''
    });
    const [editing, setEditing] = useState(false);

    //Chargement ad si besoin au chargement du composant ou au changement de l'identifiant
    useEffect(() => {
        if(state == 'edition')
        {
            setEditing(true);
            fetchAdress();
            // fetchPictures();
        } else {
            setLoading(false);
        }
    }, [uri])


    const fetchAdress = async() => {
        try {
            const {id, cp, city, street, streetNumber, country} = await locationAPI.fetchLocationByAd(uri);
            setLocation({id, cp, city, street, streetNumber, country});
            setLoading(false);

        } catch (error) {
            console.log(error.response);
            toast.error("L'adresse n'a pas pu être récupéré")
        }
    }

    
 
    //Gestion des changements des inputs dans le formulaire
    const handleChange = ({ currentTarget}) => {

        const {name, value, type} = currentTarget;
        console.log({name, value, type});

        if(type == "number")
        {
            const number = parseInt(value);
            console.log(number);
            setLocation({ ...location, [name]: number });
        }else {
            setLocation({ ...location, [name]: value });
        }
        // setLocation({ ...location, [user]: ""});
    };

    // Gestion de la soumission du formulaire
    const handleSubmit = async event => {
        //preventDefault permet de ne pas recharger la page
        event.preventDefault();
        console.log(location);
        setLoading(true);

        
        try{
            if(editing){
                console.log(location.id);
                const putLocation = await locationAPI.update(location.id, location);
                console.log(putLocation);
            }
            else {
                const postLocation = await locationAPI.post(location);
                const updateAd = await adsAPI.update(id, {location: "/api/locations/" + postLocation.id});
            }

            toast.success("Le formulaire a bien été pris en compte");
            history.replace("/administration/");
        }
        catch({response}){
            console.log(response);
            // const {violations} = response.data;
            // setLoading(false);
            // if(violations){
            //     const apiErrors = {};
            //     violations.forEach(({ propertyPath, message}) => {
            //         apiErrors[propertyPath] = message;
            //     });
            //     setErrors(apiErrors);
            // }
            toast.error("Une erreur est survenue");
        }
    };
    


    //TODO : Faire les validations, pour les numéros pas plus de 5 chiffres
    return ( 
        <>
            <title>PucesNautique - Création d'une nouvelle adresse</title>
            <div className="container mt-5">
                <ProgressBar currentStep={4}/>
                {!editing &&  <h1>Création d'une adresse</h1> || <h1>Modification d'une adresse</h1>}
                {!loading && (
                <>
                    <form onSubmit={handleSubmit}>
                        <div className="row">
                            <div className="col-3 pl-0">
                                <Field
                                    name="streetNumber"
                                    label="Numéro de rue*"
                                    placeholder="14 ..."
                                    type="number"
                                    error={errors.streetNumber}
                                    value={location.streetNumber}
                                    onChange={handleChange}
                                />
                            </div>
                            <div className="col-9 pr-0">
                                <Field
                                    name="street"
                                    label="Nom de la rue*"
                                    placeholder="rue du domaine"
                                    error={errors.street}
                                    value={location.street}
                                    onChange={handleChange}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-6 pl-0">
                                <Field
                                    name="city"
                                    label="Ville*"
                                    placeholder="Fécamp ..."
                                    error={errors.city}
                                    value={location.city}
                                    onChange={handleChange}
                                />
                            </div>
                            <div className="col-6 pr-0">
                                <Field
                                    name="cp"
                                    label="Code postal*"
                                    placeholder="76400 ..."
                                    type="number"
                                    error={errors.cp}
                                    value={location.cp}
                                    onChange={handleChange}
                                />
                            </div>
                        </div>

                        <Field
                            name="country"
                            label="Pays*"
                            placeholder="France ..."
                            error={errors.country}
                            value={location.country}
                            onChange={handleChange}
                        />


                        <div className="form-group">
                            <button 
                                type="submit" 
                                className="btn btn-success">Enregistrer les modifications
                            </button>
                            <Link to="/administration" className="btn btn-link">Je ne souhaite pas créer de nouvelle adresse</Link>
                        </div>
                    </form>
                </>
                )}
                {loading && (<FormContentLoader/>)}
            </div>
        </>
     );
}
 
export default Profil_adminPage;