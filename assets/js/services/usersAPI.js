import axios from "axios";
import adsAPI from "./adsAPI";
import jwtDecode from "jwt-decode";


/*
* Requete HTTP d'authentification et stockage du token dans axios
*/
function connexion(credentials){
    return axios     
    .post("http://127.0.0.1:8000/api/login_check", credentials)
    .then(response => response.data.token)
    .then(token => {
        window.localStorage.setItem("authToken", token);
        axios.defaults.headers["Authorization"] = 'Bearer ' + token;
        adsAPI.findAll().then(console.log);
    });
}

//Déconnexion 
//Je vide le tocken 
//Je surpprime le type d'authorization
function logout(){
    window.localStorage.removeItem("authToken");
    delete axios.defaults.headers["Authorization"];
}
 
function setAxiosToken(token) {
    axios.defaults.headers["Authorization"] = 'Bearer ' + token;
}

/**
 * Mise en place lors du chargement de l'application
 */
function setup(){
    // Voir si on a un token
    const token = window.localStorage.getItem("authToken");
    // Si le token est encore actif
    if(token){
        const { exp: expiration } = jwtDecode(token);
        if(expiration *1000 > new Date().getTime())
        {
            setAxiosToken(token);
        } 
    }
}

/**
 * Permet de savoir si on est authentifié ou pas 
 * Return booléans
 */
function isAuthenticated(){
    // Voir si on a un token
    const token = window.localStorage.getItem("authToken");
    // Si le token est encore actif
    if(token){
        const { exp: expiration } = jwtDecode(token);
        if(expiration *1000 > new Date().getTime())
        {
            return true;
        } 
        return false;
    }
    return false;
}

function post(user){
    axios.post('http://127.0.0.1:8000/api/users', user)
}

function getUser(){

    // Voir si on a un token
    const token = window.localStorage.getItem("authToken");
    // Si le token est encore actif
    if(token){
        const { exp: expiration,  id: id } = jwtDecode(token);
        if(expiration *1000 > new Date().getTime())
        {
            return axios.get('http://127.0.0.1:8000/api/users/' + id)
            .then(response => response.data);
        } 
        return false;
    }
    return false;
}


function getLocationByUser(){

    // Voir si on a un token
    const token = window.localStorage.getItem("authToken");
    // Si le token est encore actif
    if(token){
        const { exp: expiration,  id: id } = jwtDecode(token);
        if(expiration *1000 > new Date().getTime())
        {
            return axios.get('http://127.0.0.1:8000/api/users/' + id + "/locations")
            .then(response => response.data["hydra:member"]);
        } 
        return false;
    }
    return false;
}


function getAdsByUser(){

    // Voir si on a un token
    const token = window.localStorage.getItem("authToken");
    // Si le token est encore actif
    if(token){
        const { exp: expiration,  id: id } = jwtDecode(token);
        if(expiration *1000 > new Date().getTime())
        {
            return axios.get('http://127.0.0.1:8000/api/users/' + id + "/ads")
            .then(response => response.data["hydra:member"]);
        } 
        return false;
    }
    return false;
}

function update(id, user){
    return axios.put("http://127.0.0.1:8000/api/users/" + id, user);
}



export default {
    connexion,
    logout,
    setup,
    isAuthenticated,
    post,
    getUser,
    update,
    getLocationByUser,
    getAdsByUser
};