import axios from "axios";

function findAll(){
    return axios     
    .get("http://127.0.0.1:8000/api/pictures")
    .then(response => response.data["hydra:member"]);
}


function findByAd(id){
    return axios     
    .get("http://127.0.0.1:8000/api/ads/" + id + "/pictures")
    .then(response => response.data["hydra:member"]);
}

function totalItems(){
    return axios     
    .get("http://127.0.0.1:8000/api/pictures")
    .then(response => response.data["hydra:totalItems"]);
}

function findPage(itemsPerPage, currentPage){
    return axios     
    .get("http://127.0.0.1:8000/api/pictures?pagination=true&count=" + itemsPerPage + "&page=" + currentPage)
    .then(response => response.data["hydra:member"]);
}

function deletePicture(id) {
    return axios.delete("http://127.0.0.1:8000/api/pictures/" + id)
}


function postPicture(picture){
    return axios.post('http://127.0.0.1:8000/api/pictures', picture)
}

export default {
    postPicture, 
    findByAd
}