import axios from "axios";

function findAll(){
    return axios     
    .get("http://127.0.0.1:8000/api/ads")
    .then(response => response.data["hydra:member"]);
}

function totalItems(){
    return axios     
    .get("http://127.0.0.1:8000/api/ads")
    .then(response => response.data["hydra:totalItems"]);
}

function findPage(itemsPerPage, currentPage){
    return axios     
    .get("http://127.0.0.1:8000/api/ads?pagination=true&count=" + itemsPerPage + "&page=" + currentPage)
    .then(response => response.data["hydra:member"]);
}

function deleteAd(id) {
    return axios.delete("http://127.0.0.1:8000/api/ads/" + id)
}

function postAd(ad){
    return axios.post("http://127.0.0.1:8000/api/ads", ad)
    .then(response => response.data);
}

function fetchAd(id){
    return axios
    .get("http://127.0.0.1:8000/api/ads/" + id)
    .then(response => response.data);
}

function update(id, ad){
    return axios.put("http://127.0.0.1:8000/api/ads/" + id, ad)
    .then(response => response.data);
}

function fetchCategoryByAd(id){
    return axios
    .get("http://127.0.0.1:8000/api/ads/" + id + "/category/")
    .then(response => response.data);
}

function getMarker(city){
    return axios.get("http://nominatim.openstreetmap.org/search?format=json&limit=1&q=" + city)
    .then(response => response.data);;
}
function getPictures(id){
    return axios.get("http://127.0.0.1:8000/api/ads/" + id + "/pictures/")
    .then(response => response.data["hydra:member"]);
}

export default {
    findAll,
    delete: deleteAd,
    create: postAd,
    find: fetchAd,
    update,
    findPage: findPage,
    totalItems: totalItems,
    fetchCategoryByAd,
    getMarker,
    getPictures
}