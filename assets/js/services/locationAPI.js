import axios from "axios";

function findAll(){
    return axios     
    .get("http://127.0.0.1:8000/api/locations")
    .then(response => response.data["hydra:member"]);
}

function totalItems(){
    return axios     
    .get("http://127.0.0.1:8000/api/locations")
    .then(response => response.data["hydra:totalItems"]);
}

function findPage(itemsPerPage, currentPage){
    return axios     
    .get("http://127.0.0.1:8000/api/locations?pagination=true&count=" + itemsPerPage + "&page=" + currentPage)
    .then(response => response.data["hydra:member"]);
}

function deleteLocation(id) {
    return axios.delete("http://127.0.0.1:8000/api/locations/" + id)
}

function postLocation(location){
    return axios.post("http://127.0.0.1:8000/api/locations", ad)
    
}

function fetchLocation(id){
    return axios
    .get("http://127.0.0.1:8000/api/locations/" + id)
    .then(response => response.data);
}

function update(id, location){
    return axios.put("http://127.0.0.1:8000/api/locations/" + id, location);
}

function post(location){
    return axios.post('http://127.0.0.1:8000/api/locations', location)
    .then(response => response.data);
}

function fetchLocationByAd(id){
    return axios
    .get("http://127.0.0.1:8000/api/ads/"+ id +"/location")
    .then(response => response.data);
}

export default {
    findAll,
    update,
    findPage: findPage,
    totalItems: totalItems,
    post, 
    fetchLocation,
    fetchLocationByAd,
    update
}