//Les imports important
import React, {useState} from 'react';
import ReactDOM from 'react-dom'

import { HashRouter, Switch, Route, withRouter} from "react-router-dom";
import PrivateRoute from './components/PrivateRoute';
import { ToastContainer, toast } from 'react-toastify';

// import CustomersPage from './pages/CustomersPage';
// import CustomerPage from './pages/CustomerPage';
// import InvoicesPage from './pages/InvoicesPage';

import usersAPI from './services/usersAPI';
import AuthContext from './contexts/AuthContext';

import NavBar from './components/Navbar';
import Footer from './components/Footer';

import HomePage from './pages/HomePage';
import StoryPage from './pages/StoryPage';
import ContactPage from './pages/ContactPage';


import AdsPage from './pages/ads/AdsPage';
import AdViewPage from './pages/ads/AdViewPage';


import Ads_adminPage from './pages/administration/Ads_adminPage';
import Caract_formPage from './pages/administration/form/Caract_formPage';
import Ads_formPage from './pages/administration/form/Ads_formPage';
import Profil_adminPage from './pages/administration/Profil_adminPage';

import Adress_formPage from './pages/administration/form/Adress_formPage';
import Pictures_formPage from './pages/administration/form/Pictures_formPage';

import RegisterPage from './pages/connexion/RegisterPage';
import LoginPage from './pages/connexion/LoginPage';

import "react-toastify/dist/ReactToastify.css";
require('../css/app.css');

usersAPI.setup();

// Route réellement différente 

const App = () => {
    const [isAuthenticated, setIsAuthenticated] = useState(
        usersAPI.isAuthenticated()
    );


    const NavBarWithRouter = withRouter(NavBar);

    return <>
        <AuthContext.Provider value={{
            isAuthenticated,
            setIsAuthenticated
        }}>
            <HashRouter>
                <NavBarWithRouter/>
                <main className=" ">
                    <Switch>
                        <Route path="/login" component={LoginPage} />
                        <Route path="/register" component={RegisterPage} />

                        <PrivateRoute path="/administration/profil" component={Profil_adminPage}/>
                        <PrivateRoute path="/administration" component={Ads_adminPage}/>

                        <PrivateRoute path="/annonce/formulaire/:id" component={Ads_formPage}/>
                        <PrivateRoute path="/annonce/caracteristique/:uri/:state" component={Caract_formPage}/>
                        <PrivateRoute path="/annonce/images/:id/:state" component={Pictures_formPage}/>
                        <PrivateRoute path="/annonce/adresse/:uri/:state" component={Adress_formPage}/>

                        <Route path="/annonce/:id" component={AdViewPage}/>
                        <Route path="/annonces" component={AdsPage}/>

                        <Route path="/histoire" component={StoryPage}/>
                        <Route path="/contact" component={ContactPage}/>



                        {/* <PrivateRoute path="/invoices/:id" component={InvoicePage}/>
                        <PrivateRoute path="/invoices" component={InvoicesPage}/>
                        <PrivateRoute path="/customers/:id" component={CustomerPage}/>
                        <PrivateRoute path="/customers"component={CustomersPage}/> */}
                        <Route path="/" component={HomePage}/>
                    </Switch>
                    <Footer/>
                </main>
            </HashRouter>
            <ToastContainer position={toast.POSITION.BOTTOM_LEFT}/>
        </AuthContext.Provider>
        
    </>
};

const rootElement = document.querySelector('#app');
ReactDOM.render(<App />, rootElement);