import React from 'react';

{/* <Pagination 
currentPage={currentPage} 
itemsPerPage={itemsPerPage} 
length={customers.length} 
onPageChanged={handlePageChange
} /> */}

const Pagination = ({currentPage, itemsPerPage, length, onPageChanged}) => {
    
    const pagesCount = Math.ceil(length / itemsPerPage);
    const pages = [];

    const pagination = 25;

    const nbPage = currentPage + pagination;


    if (nbPage < pagesCount) {
        for(let i = 1 ; i <= nbPage; i++)
        {
            pages.push(i);
        }
    }else {
        for(let i = 1 ; i <= pagesCount; i++)
        {
            pages.push(i);
        }
    }


    // const pages = allPages.slice(0, 25);
    if (currentPage !== 1 | 2) {
        pages.splice(0, currentPage - 2);
    }

    return (  
        <div>
            <ul className="pagination pagination-sm">
                <li className={"page-item" + (currentPage === 1 && " disabled")}>
                    <button className="page-link" onClick={() => onPageChanged(currentPage - 1)}>&laquo;</button>
                </li>
                {
                pages.map(page => (
                    <li key={page} className={"page-item " + (currentPage === page && " active")}>
                        <button className="page-link " onClick={() => onPageChanged(page)} >{page}</button>
                    </li>
                ))}
                <li className={"page-item" + (currentPage === pagesCount && " disabled")}>
                    <button className="page-link" onClick={() => onPageChanged(currentPage + 1)}>&raquo;</button>
                </li>
            </ul>
        </div>
    );
}

Pagination.getData = (items, currentPage, itemsPerPage) => {
    const start = currentPage * itemsPerPage - itemsPerPage;
    return items.slice(start, start + itemsPerPage);
}

export default Pagination;

