import React, { useContext } from "react";
import usersAPI from "../services/usersAPI";
import { NavLink } from "react-router-dom";
import AuthContext from "../contexts/AuthContext";
import { toast } from "react-toastify";
import { DropdownButton, Dropdown, ButtonGroup, Button  } from 'react-bootstrap';


const NavBar = ({ history }) => {
  const { isAuthenticated, setIsAuthenticated } = useContext(AuthContext);

  const handleLogout = () => {
    usersAPI.logout();
    setIsAuthenticated(false);
    toast.info("Vous êtes désormais déconnecté");
    history.push("/login");
  };


  //TODO: Gérer le dropdown en responsive
  
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <NavLink className="navbar-brand" to="/">
        PucesNautiques
      </NavLink>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarColor01"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarColor01">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <NavLink className="nav-link" to="/annonces">
              Annonces
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/histoire">
              L'histoire des puces nautiques
            </NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/contact">
                Contactez-nous
              </NavLink>
            </li> 
        </ul>

        <ul className="navbar-nav ml-auto">
          
          <li className="nav-item">
            <NavLink className="nav-link" to="/annonce/formulaire/new">
              Créer une annonce
            </NavLink> 
          </li>

          {(!isAuthenticated && (
            <>
              <li className="nav-item">
                <NavLink to="/register" className="nav-link btn btn-warning">
                  Inscription
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/login" className="nav-link btn btn-info">
                  Connexion
                </NavLink>
              </li>
            </>
          )) || (
            <>

              <li className="nav-item">
                <NavLink className="nav-link" to="/administration">
                  Mes annonces
                </NavLink> 
              </li> 
              <li className="nav-item">
                <NavLink className="nav-link" to="/administration/profil">
                  Mon profil
                </NavLink> 
              </li> 

              <li className="nav-item">
                <button onClick={handleLogout} className="nav-link btn btn-warning ">
                  Déconnexion
                </button>
              </li>
            </>
          )}
        </ul>
      </div>
    </nav>
  );
};

export default NavBar;
