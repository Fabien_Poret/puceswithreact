import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {

    // TODO: Fixer le footer 
    return ( 
        <>
            <footer className="page-footer font-small unique-color-dark">
                <div className="container text-center text-md-left mt-5">
                    <div className="row mt-3">
                        <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                            <h6 className="text-uppercase font-weight-bold">CANOE-SHOP</h6>
                            <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto"/>
                            <p>
                                Canoe Shop est le spécialiste de la vente en ligne de canoës-kayaks. 
                                La boutique travaille avec les plus grandes marques pour vous proposer une sélection des meilleures références du marché.
                            </p>
                        </div>
                        <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 className="text-uppercase font-weight-bold">PucesNautiques</h6>
                            <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto"/>
                            <p>
                                <Link to="/annonce/formulaire/new">Déposer une annonce</Link>
                            </p>
                            <p>
                                <a target="_blank" href="https://www.canoe-shop.com/content/4-qui-sommes-nous">Qui sommes-nous ?</a>
                            </p>
                            <p>
                                <a target="_blank" href="https://www.canoe-shop.com/content/11-nos-engagements">Nos engagements</a>
                            </p>
                            <p>
                                <a target="_blank" href="https://www.canoe-shop.com/content/4-qui-sommes-nous">Découvrir le shop</a>
                            </p>
                        </div>
                        <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 className="text-uppercase font-weight-bold">Informations légales</h6>
                            <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" />
                            <p>
                                <a target="_blank" href="https://www.canoe-shop.com/content/2-mentions-legales">Mentions légales</a>
                            </p>
                            <p>
                                <a target="_blank" href="https://www.canoe-shop.com/content/3-conditions-generales-de-vente">Conditions de ventes</a>
                            </p>
                            <p>
                                <a target="_blank" href="https://www.canoe-shop.com/content/10-privacy-policy">Politique de confidentialité</a>
                            </p>
                            <p>
                                <a target="_blank" href="https://www.canoe-shop.com/plan-site">Plan du site</a>
                            </p>
                        </div>
                        <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                            <h6 className="text-uppercase font-weight-bold">Informations de contact</h6>
                            <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto"/>

                    
                            <p> 
                                <i className=""></i> Allée des Grands Prés, 14160 Dives-sur-Mer - France</p>
                            <p>
                                <i className=""></i>info@canoe-shop.com</p>
                            <p>
                                <i className=""></i>02 31 28 73 22</p>
                        </div>
                    </div>
                </div>

            </footer>
        </>
     );
}
 
export default Footer;