import React from 'react';
import '../../css/progressBar.css';


const ProgressBar = ({currentStep}) => {
    return (
        <div className="row">
            <ul className="progressbar">
                {(currentStep == 1 && (
                    <>
                        <li className="">Création de l'annonce</li>
                        <li className="">Caractéristique du produit</li>
                        <li className="">Les images du produit</li>
                        <li className="">L'adresse de l'annonce</li>
                    </>
                ))}
                {(currentStep == 2 && (
                    <>
                        <li className="active">Création de l'annonce</li>
                        <li className="">Caractéristique du produit</li>
                        <li className="">Les images du produit</li>
                        <li className="">L'adresse de l'annonce</li>
                    </>
                ))}
                
                {(currentStep == 3 && (
                    <>
                        <li className="active">Création de l'annonce</li>
                        <li className="active">Caractéristique du produit</li>
                        <li className="">Les images du produit</li>
                        <li className="">L'adresse de l'annonce</li>
                    </>
                ))}
                {(currentStep == 4 && (
                    <>
                        <li className="active">Création de l'annonce</li>
                        <li className="active">Caractéristique du produit</li>
                        <li className="active">Les images du produit</li>
                        <li className="">L'adresse de l'annonce</li>
                    </>
                ))}
            </ul>
        </div>
        
    );
  };

  export default ProgressBar;