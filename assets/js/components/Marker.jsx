import React from 'react';
import '../../css/marker.css';


//TODO: Faire le CSS pour le marker

const Marker = (props) => {
    const { color, name, id } = props;
    return (
      <div className="marker"
        style={{ backgroundColor: color, cursor: 'pointer'}}
        title={name}
      />
    );
  };

  export default Marker;