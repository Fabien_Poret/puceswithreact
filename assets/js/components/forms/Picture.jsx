import React from 'react';

/**
 * Name
 * Label
 * Value
 * OnChange
 * PlaceHolder
 * Type
 * Error
 */

const Picture = ({ name, label, value, placeholder, onChange, error = ""}) => {
    return (

        <div className="form-group">
            <div className="input-group mb-3">
                <div className="custom-file">
                    <input 
                        type="file" 
                        placeholder={placeholder}
                        name={name}
                        id={name}
                        value={value} 
                        onChange={onChange} 
                        className={"custom-file-input " + (error && " is-invalid")}
                    />
                    <label htmlFor={name}
                    className={"custom-file-label " + (error && " is-invalid")}>
                        {label}
                    </label>
                </div>
                <div className="input-group-append">
                    <span className="input-group-text" id="">Télecharger</span>
                </div>
            </div>
        {error && 
            <p className="invalid-feedback">
                {error}
            </p>
        } 
    </div>
    );
}
 
export default Picture;

