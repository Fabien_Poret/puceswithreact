<?php
 
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{    
    /**
     * @Route("/", name="app")
     */
    public function index()
    {
        header("Access-Control-Allow-Origin: *");

        return $this->render('app/index.html.twig', [
            'controller_name' => 'AppController',
        ]);
    }
}
