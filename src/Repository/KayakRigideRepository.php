<?php

namespace App\Repository;

use App\Entity\KayakRigide;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method KayakRigide|null find($id, $lockMode = null, $lockVersion = null)
 * @method KayakRigide|null findOneBy(array $criteria, array $orderBy = null)
 * @method KayakRigide[]    findAll()
 * @method KayakRigide[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KayakRigideRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KayakRigide::class);
    }

    // /**
    //  * @return KayakRigide[] Returns an array of KayakRigide objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KayakRigide
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findKayakByIdAd($id){

        dump($id);
        
        return $this->createQueryBuilder("SELECT * from kayak_rigide as k
        inner join product_type_kayak_rigide as pk on k.id = pk.kayak_rigide_id
        inner join product_type as p on pk.product_type_id = p.id
        inner join ad as a on p.id = a.product_type_id
        where a.id = " . $id );
    }
}
