<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200122130616 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE kayak_rigide (id INT AUTO_INCREMENT NOT NULL, place_number INT NOT NULL, weight DOUBLE PRECISION DEFAULT NULL, length DOUBLE PRECISION DEFAULT NULL, width DOUBLE PRECISION DEFAULT NULL, max_charge INT DEFAULT NULL, unsinkable TINYINT(1) DEFAULT NULL, water_drain TINYINT(1) DEFAULT NULL, approval_div245 TINYINT(1) DEFAULT NULL, activity VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_type (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_type_kayak_rigide (product_type_id INT NOT NULL, kayak_rigide_id INT NOT NULL, INDEX IDX_96A76C5614959723 (product_type_id), INDEX IDX_96A76C5666E6A2B3 (kayak_rigide_id), PRIMARY KEY(product_type_id, kayak_rigide_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_type_kayak_rigide ADD CONSTRAINT FK_96A76C5614959723 FOREIGN KEY (product_type_id) REFERENCES product_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_type_kayak_rigide ADD CONSTRAINT FK_96A76C5666E6A2B3 FOREIGN KEY (kayak_rigide_id) REFERENCES kayak_rigide (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ad ADD product_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ad ADD CONSTRAINT FK_77E0ED5814959723 FOREIGN KEY (product_type_id) REFERENCES product_type (id)');
        $this->addSql('CREATE INDEX IDX_77E0ED5814959723 ON ad (product_type_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_type_kayak_rigide DROP FOREIGN KEY FK_96A76C5666E6A2B3');
        $this->addSql('ALTER TABLE ad DROP FOREIGN KEY FK_77E0ED5814959723');
        $this->addSql('ALTER TABLE product_type_kayak_rigide DROP FOREIGN KEY FK_96A76C5614959723');
        $this->addSql('DROP TABLE kayak_rigide');
        $this->addSql('DROP TABLE product_type');
        $this->addSql('DROP TABLE product_type_kayak_rigide');
        $this->addSql('DROP INDEX IDX_77E0ED5814959723 ON ad');
        $this->addSql('ALTER TABLE ad DROP product_type_id');
    }
}
