<?php

namespace App\Events;

use App\Entity\Ad;
use App\Entity\Location;
use App\Entity\User;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security as SymfonySecurity;


class LocationSubscriber implements EventSubscriberInterface
{

    private $security;
    public function __construct(SymfonySecurity $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['locationEvent', EventPriorities::PRE_WRITE]
        ];
    }

    public function locationEvent(ViewEvent $event)
    {
        $result = $event->getControllerResult();
        $method = $event->getRequest()->getMethod(); // POST, GET, PUT, ...

        // Si result est une instance de user
        if ($result instanceof Location && $method === "POST") {
            $user = $this->security->getUser();
            $result->setUser($user);
        }
    }
}
