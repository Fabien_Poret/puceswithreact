<?php 

namespace App\Events;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JwtCreatedSubscriber {

    public function updateJwtData(JWTCreatedEvent $event)
    {

        // Je récupère mon user
        $user = $event->getUser();
        
        // Je lui assigne les données du token
        $data = $event->getData();

        // J'ajoute l'id de l'users dans les data du token
        $data['id'] = $user->getId();
        
        // Je créer le token
        $event->setData($data);
    }
} 
