<?php

namespace App\Events;

use App\Entity\Ad;
use App\Entity\User;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security as SymfonySecurity;


class AdSubscriber implements EventSubscriberInterface
{

    private $security;
    public function __construct(SymfonySecurity $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['adEvent', EventPriorities::PRE_WRITE]
        ];
    }

    public function adEvent(ViewEvent $event)
    {


        $result = $event->getControllerResult();
        $method = $event->getRequest()->getMethod(); // POST, GET, PUT, ...

        // Si result est une instance de user
        if ($result instanceof Ad && $method === "POST") {
            $user = $this->security->getUser();
            $dateTime = new \DateTime();
            $result->setCreatedAt($dateTime);
            $result->setUser($user);
        }
    }
}
