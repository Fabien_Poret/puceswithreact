<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 * @ApiResource(
 *  normalizationContext={
 *      "groups"={"location_read"}
 *  }
 * )
 */
class Location
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"ads_read", "location_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"ads_read", "location_read"})
     * @Assert\NotBlank(message="Le code postal est obligatoire")
     * @Assert\Type(type="numeric", message="Le code postale doit être un nombre !")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"ads_read", "location_read"})
     */
    private $city;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"ads_read", "location_read"})
     * @Assert\NotBlank(message="Le code postal est obligatoire")
     * @Assert\Type(type="numeric", message="Le code postale doit être un nombre !")
     */
    private $streetNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"ads_read", "location_read"})
     */
    private $street;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ad", mappedBy="location")
     * @Groups({"location_read"})
     * @ApiSubresource
     */
    private $ad;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"ads_read", "location_read"})
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="locations")
     * @Groups({"location_read"})
     */
    private $user;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"ads_read", "location_read"})
     */
    private $defaultAdress;

    public function __construct()
    {
        $this->ad = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreetNumber(): ?int
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(?int $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return Collection|ad[]
     */
    public function getAd(): Collection
    {
        return $this->ad;
    }

    public function addAd(ad $ad): self
    {
        if (!$this->ad->contains($ad)) {
            $this->ad[] = $ad;
            $ad->setLocation($this);
        }

        return $this;
    }

    public function removeAd(ad $ad): self
    {
        if ($this->ad->contains($ad)) {
            $this->ad->removeElement($ad);
            // set the owning side to null (unless already changed)
            if ($ad->getLocation() === $this) {
                $ad->setLocation(null);
            }
        }

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDefaultAdress(): ?bool
    {
        return $this->defaultAdress;
    }

    public function setDefaultAdress(?bool $defaultAdress): self
    {
        $this->defaultAdress = $defaultAdress;

        return $this;
    }
}
