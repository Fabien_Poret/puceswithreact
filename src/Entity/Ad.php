<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;


/**
 * @ORM\Entity(repositoryClass="App\Repository\AdRepository")
 * @ApiResource(
 *  attributes={
 *      "order": {"createdAt":"desc"}
 *  },
 *  collectionOperations={"GET", "POST"},
 *  itemOperations={"GET", "PUT", "DELETE"},
 *  normalizationContext={
 *      "groups"={"ads_read"}
 *  },
 * denormalizationContext={"disable_type_enforcement"=true},
 * )
 * @ApiFilter(SearchFilter::class, properties={"id":"exact", "price":"exact", "title": "partial", "brand": "partial", "productType": "partial"})
 */
class Ad
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"ads_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"ads_read"})
     * @Assert\NotBlank(message="Le titre de l'annonce est obligatoire")
     * @Assert\Length(min=10, minMessage="Le titre de l'annonce doit faire entre 10 et 255 caractères", max=255, minMessage="Le titre de l'annonce doit faire entre 10 et 255 caractères")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"ads_read"})
     * @Assert\NotBlank(message="La description est obligatoire")
     * @Assert\Length(min=5, minMessage="La description doit faire plus de 5 caractères")
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     * @Groups({"ads_read"})
     * @Assert\NotBlank(message="Le montant du prix de l'annonce est obligatoire")
     * @Assert\Type(type="numeric", message="Le montant du prix de l'annonce doit être un nombre !")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"ads_read"})
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="ad")
     * @Groups({"ads_read"})
     * @Assert\NotBlank(message="Le choix d'une catégorie d'annonce est obligatoire")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", inversedBy="ad")
     * @Groups({"ads_read"})
     * @ApiSubresource
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductType", inversedBy="ad")
     * @Groups({"ads_read"})
     */
    private $productType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ads")
     * @Groups({"ads_read"})
     */
    private $user;

    /**
     * @Groups({"ads_read"})
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Picture", mappedBy="ad")
     * @Groups({"ads_read"})
     */
    private $pictures;

    public function __construct()
    {
        $this->pictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getProductType(): ?ProductType
    {
        return $this->productType;
    }

    public function setProductType(?ProductType $productType): self
    {
        $this->productType = $productType;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setAd($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->contains($picture)) {
            $this->pictures->removeElement($picture);
            // set the owning side to null (unless already changed)
            if ($picture->getAd() === $this) {
                $picture->setAd(null);
            }
        }

        return $this;
    }
}
