<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductTypeRepository")
 * @ApiResource(
 *  normalizationContext={
 *      "groups"={"productType_read"}
 *  }
 * )
 */
class ProductType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"ads_read", "productType_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"ads_read", "productType_read"})
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\KayakRigide", inversedBy="productTypes")
     * @Groups({"ads_read", "productType_read"})
     */
    private $kayakrigide;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ad", mappedBy="productType")
     * @Groups({"productType_read"})
     */
    private $ad;

    public function __construct()
    {
        $this->kayakrigide = new ArrayCollection();
        $this->ad = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|KayakRigide[]
     */
    public function getKayakrigide(): Collection
    {
        return $this->kayakrigide;
    }

    public function addKayakrigide(KayakRigide $kayakrigide): self
    {
        if (!$this->kayakrigide->contains($kayakrigide)) {
            $this->kayakrigide[] = $kayakrigide;
        }

        return $this;
    }

    public function removeKayakrigide(KayakRigide $kayakrigide): self
    {
        if ($this->kayakrigide->contains($kayakrigide)) {
            $this->kayakrigide->removeElement($kayakrigide);
        }

        return $this;
    }

    /**
     * @return Collection|Ad[]
     */
    public function getAd(): Collection
    {
        return $this->ad;
    }

    public function addAd(Ad $ad): self
    {
        if (!$this->ad->contains($ad)) {
            $this->ad[] = $ad;
            $ad->setProductType($this);
        }

        return $this;
    }

    public function removeAd(Ad $ad): self
    {
        if ($this->ad->contains($ad)) {
            $this->ad->removeElement($ad);
            // set the owning side to null (unless already changed)
            if ($ad->getProductType() === $this) {
                $ad->setProductType(null);
            }
        }

        return $this;
    }
}
