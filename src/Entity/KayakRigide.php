<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KayakRigideRepository")
 * @ApiResource(
 *  normalizationContext={
 *      "groups"={"kayakRigide_read"}
 *  }
 * )
 */
class KayakRigide
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"ads_read", "productType_read", "kayakRigide_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"ads_read", "productType_read",  "kayakRigide_read"})
     */
    private $placeNumber;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"ads_read", "productType_read",  "kayakRigide_read"})
     */
    private $weight;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"ads_read", "productType_read", "kayakRigide_read"})
     */
    private $length;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"ads_read", "productType_read", "kayakRigide_read"})
     */
    private $width;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"ads_read", "productType_read", "kayakRigide_read"})
     */
    private $maxCharge;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"ads_read", "productType_read", "kayakRigide_read"})
     */
    private $unsinkable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"ads_read", "productType_read", "kayakRigide_read"})
     */
    private $waterDrain;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"ads_read", "productType_read", "kayakRigide_read"})
     */
    private $approvalDiv245;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"ads_read", "productType_read", "kayakRigide_read"})
     */
    private $activity;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProductType", mappedBy="kayakrigide")
     * @Groups({"kayakRigide_read"})
     */
    private $productTypes;

    public function __construct()
    {
        $this->productTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlaceNumber(): ?int
    {
        return $this->placeNumber;
    }

    public function setPlaceNumber(int $placeNumber): self
    {
        $this->placeNumber = $placeNumber;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getLength(): ?float
    {
        return $this->length;
    }

    public function setLength(?float $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(?float $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getMaxCharge(): ?int
    {
        return $this->maxCharge;
    }

    public function setMaxCharge(?int $maxCharge): self
    {
        $this->maxCharge = $maxCharge;

        return $this;
    }

    public function getUnsinkable(): ?bool
    {
        return $this->unsinkable;
    }

    public function setUnsinkable(?bool $unsinkable): self
    {
        $this->unsinkable = $unsinkable;

        return $this;
    }

    public function getWaterDrain(): ?bool
    {
        return $this->waterDrain;
    }

    public function setWaterDrain(?bool $waterDrain): self
    {
        $this->waterDrain = $waterDrain;

        return $this;
    }

    public function getApprovalDiv245(): ?bool
    {
        return $this->approvalDiv245;
    }

    public function setApprovalDiv245(?bool $approvalDiv245): self
    {
        $this->approvalDiv245 = $approvalDiv245;

        return $this;
    }

    public function getActivity(): ?string
    {
        return $this->activity;
    }

    public function setActivity(?string $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * @return Collection|ProductType[]
     */
    public function getProductTypes(): Collection
    {
        return $this->productTypes;
    }

    public function addProductType(ProductType $productType): self
    {
        if (!$this->productTypes->contains($productType)) {
            $this->productTypes[] = $productType;
            $productType->addKayakrigide($this);
        }

        return $this;
    }

    public function removeProductType(ProductType $productType): self
    {
        if ($this->productTypes->contains($productType)) {
            $this->productTypes->removeElement($productType);
            $productType->removeKayakrigide($this);
        }

        return $this;
    }
}
