<?php

namespace App\DataFixtures;

use App\Entity\Ad;
use Faker\Factory;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\Location;
use App\Entity\KayakRigide;
use App\Entity\Picture;
use App\Entity\ProductType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        
        for ($c=1; $c < 5; $c++) { 

            $category = new Category();
            $category->setTitle('Catégorie numéro : ' . $c)
                    ->setDescription($faker->sentence(50));
            $manager->persist($category);
            
            for ($u=1; $u < 5; $u++) { 
                $user = new User();
                $user->setEmail($faker->email())
                    ->setFirstName($faker->firstName())
                    ->setLastName($faker->lastName())
                    ->setPassword("admin");

                $manager->persist($user);
            
                for ($l=1; $l < 5; $l++) { 
                    
                    $location = new Location();
                    $location->setCp($c . $l)
                            ->setCity($faker->city())
                            ->setStreetNumber($faker->buildingNumber())
                            ->setStreet($faker->streetName())
                            ->setCountry($faker->country())
                            ->setUser($user);
                    $manager->persist($location);
                    
                    for ($k=1; $k < 5; $k++) { 
                        $kayak = new KayakRigide();
                        $kayak->setPlaceNumber($faker->randomDigit())
                            ->setWeight($faker->randomFloat())
                            ->setLength($faker->randomFloat())
                            ->setWidth($faker->randomFloat())
                            ->setMaxCharge($faker->randomDigit())
                            ->setWaterDrain($faker->boolean())
                            ->setApprovalDiv245($faker->boolean())
                            ->setActivity($faker->sentence(1));
                        $manager->persist($kayak);
                        
                        for ($p=1; $p < 3; $p++) { 
                            $productType = new ProductType();
                            $productType->setTitle('Type de produit numéro : ' . $p)
                                        ->addKayakrigide($kayak);
                            $manager->persist($productType);

                            for ($a=1; $a < 10; $a++) { 
                                $ad = new Ad();
                                $ad->setTitle('Annonce numéro : ' . $a)
                                    ->setDescription($faker->sentence(50))
                                    ->setPrice($faker->randomFloat())
                                    // ->setPicture('http://placehold.it/300x200')
                                    ->setBrand($faker->company())
                                    ->setCreatedAt($faker->dateTimeBetween($startDate = '-6 months', $endDate = 'now'))
                                    ->setLocation($location)
                                    ->setCategory($category)
                                    ->setProductType($productType)
                                    ->setUser($user);
                                $manager->persist($ad);

                                for ($i=0; $i < 3; $i++) { 
                                    # code...
                                    $picture = new Picture();
                                    $picture->setPath('http://placehold.it/300x200')
                                            ->setAd($ad);
                                    $manager->persist($picture);
                                }
                            }
                        }
                    }
                }
            }
        }

        $manager->flush();
    }
}
